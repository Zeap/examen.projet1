<?php 
session_start();
?>
<?php
	include('selectmembre.php');
	include('rang.php');
	if($membre['rang'] >= 6)
	{
		$stmt = $bdd->prepare("SELECT * FROM maintenance");
		if($stmt->execute(array()) && $row = $stmt->fetch())
	  	{
	    $etat = $row['etat']; 
	    } 
	    if ($etat == 1)
	    {
	    	$req = $bdd->prepare("UPDATE maintenance SET etat = :etat");
	    	$req->execute(array('etat' => 0));
	    	echo 'Le site n\'est plus en maintenance';

	    	header('location: admin.php');
	    }
	    else
	    {
	    	$req = $bdd->prepare("UPDATE maintenance SET etat = :etat");
	    	$req->execute(array('etat' => 1));
	    	echo 'Le site est désormais en maintenance';

	    	header('location: admin.php');
	    }
	}
	else
	{
		echo "Vous devez être connecté et avoir le rang requis pour cette fonctionnalité !";
	}

?>