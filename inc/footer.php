			<div id="footer">
				<div id="footer_contenu">
					<div id="footer_text_gauche">Site réalisé par Alexis DOMINIQUE.<br>©Copyright 2016-2017 esthetiquetatiana.com - Tous droits réservés.<br>Toute reproduction est interdite.</div>

					<div id="footer_text_droite">
						<img src="images/icon_address.png" />
						&nbsp;&nbsp;28-30 Boulevard de Maule <br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;72650 Saint-Saturnin<br>
						<img src="images/icon_phone.png" />
						&nbsp;&nbsp;09 81 74 27 34<br>
						<img src="images/icon_internet.png" />
						&nbsp;&nbsp;esth.tatiana@gmail.com
					</div>
				</div>
			</div>